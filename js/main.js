$(document).ready(function() {
    // ====== Scroll TO ===== //

  var $scrollTo = $('.scroll-to')

  $scrollTo.click(function(e) {
    e.preventDefault();
    var $to     = $(this).attr('href'),
        $offset = $($to).offset().top;
        console.log($offset, $to)

      $('html ,body').animate({
        scrollTop: $offset + 60
      }, 2000)

  });

  $('.slider').slick({
    appendDots: '.append-dots',
    prevArrow:'.slider-container .prev',
    nextArrow:'.slider-container .next',
      dots: true,
      autoplay:true,
      autoplaySpeed: 1700,
  });

  $('.slider-subpage').slick({
    prevArrow:'<button type="button" class="slick-prev"><img src="images/arrow-next.png"/></button>',
    nextArrow:'<button type="button" class="slick-next"><img src="images/arrow-prev.png"/></button>'
  });
  
  $('.mini-slider').slick({
    arrows: true,
    prevArrow:'<button type="button" class="slick-prev"><img src="images/arrow-next.png"/></button>',
    nextArrow:'<button type="button" class="slick-next"><img src="images/arrow-prev.png"/></button>'
  });
  //
  var $fitHeight    = $('.fit-height');
  var $windowHeight; 

  $windowHeight = $(window).height();
  $fitHeight.height($windowHeight)

  $(window).on('resize', function() {
    $windowHeight = $(window).height();
    $fitHeight.height($windowHeight)

  })


  $('.gallery-thumb').click(function(e) {
    e.preventDefault();
    var preview = $(".img-preview")
    var image = $(this).attr('href');

    $('.gallery-thumb').removeClass('active');
    $(this).addClass('active');
    console.log(image)
    preview.attr('src', image);

  })




})